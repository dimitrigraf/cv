# cv

My online CV.

This website just uses HTML, CSS and some JavaScript. I publish the site with [render](https://render.com/docs/static-sites) which allows me to make automatic deployments after every new commit.
